# Notes

## default.conf.tpl

- - ${xyz} here 'xyz' is a variable
- - '/etc/nginx/' is the default directory nginx expects to find the config files.
- - we direct all the requests with url '/static/' to nginx and let all the other requests go directly to the uwsgi server.

## uwsgi_params

- - All these params are required in nginx in order to successfully pass through requests.

## entrypoint.sh

- - Used to run the nginx application in docker
- - #!/bin/sh -> start of a shell script
- - set -e -> Returns the error to the screen in case of failure

- - nginx -g 'daemon off;' -> Command to run the nginx application as the primary application in the full ground and not in the background (as default). This is good for docker and all the errors are output to the docker log.

## Dockerfile

- - We use a unprivilege nginx image so that we don't have to run nginx as the root user.

- - '-p' in the 'RUN mkdir -p /vol/static' command means, keep making all the directories along the way, i.e. if vol doesn't exit then make it and move forward making other directories in line.

- - chown <user>:<group> -> change the ownership to the mentioned user and group.

## .gitlab-ci.yml

- - We need to specify a docker image that is used to run the
- - we are using a docker in docker (dind) service to run docker in the running dev environment
- - In Gitlab CI you can group jobs into different stages
- - On line 9, Build is a job running in Build stage
- - --compress flag is used in production to compress the docker image as much as possible
- - We save the docker image to the disk as we want to provide it as an artifact.
- - An artifact is way to pass files from one job to the next.
- - $(aws ecr get-login --no-include-email --region us-east-1) -> Create a docker login command for ECR. We first of run the get-login command and then the command to login. $() represents a nested command
- - rules -> it specify when we can run the scripts/ or the job.

- - A release is going to be done any times we create a release tag for out project
- - If a job doesn't use the mentioned before_scripts then just pass a black square brackets
- - We use before_script to follow the DRY principles

### Push Dev Job

- - export TAGGED_ECR_REPO=$ECR_REPO:$(echo $CI_COMMIT_TAG | sed 's/-release//') ->
- - $CI_COMMIT_TAG has a '-release' in the name and we strip the -release using the 'sed' command in linux
- - Then we append the result with the ECR_REP name.$ECR_REPO:1.0.1
- - Then we pass that into a variable name TAGGED_ECR_REPO

- - We tag the latest docker image with the version tag, i.e. $TAGGED_ECR_REPO and push it. This image shows the version
- - It is best practice to change the latest docker image , i.e. with the tag $ECR_REPO:latest, to be actually the latest. So we tag out image again and push it. This image lets the user know that it is the latest of all the images pushed so far. Some interested in the latest version would directly see this.
- - Regx Trivia: starts with '/^' and ends with '$/'. `*` is a wildcard
